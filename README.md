![État](https://framagit.org/blankoworld/blankoworld.frama.io/badges/master/build.svg)

---

Site de bilan de la Communauté des Horizons Résilients utilisant l'outil [Hugo][hugo] et les [Pages Gitlab][userpages] pour la publication.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Afficher le site chez soi](#afficher-le-site-chez-soi)
  - [Prévisualiser le site](#pr%C3%A9visualiser-le-site)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

Les pages de ce projet sont construites à l'aide de [GitLab CI][ci] en suivant 
les étapes définies dans le fichier [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Afficher le site chez soi

Pour pouvoir travailler sur le site depuis chez soi, suivez les étapes ci-dessous : 

1. Téléchargez ce projet
1. [Installez le logiciel Hugo][install]
1. Prévisualisez le site avec la commande suivante : `hugo server`
1. Ajoutez du contenu (par exemple en éditant un fichier dans le dossier `content`)
1. Générez le site : `hugo` (optionnel)

Apprenez en plus sur la [documentation][] d'Hugo.

### Prévisualiser le site

Si vous avez cloné ou téléchargé ce projet sur votre ordinateur local, et lancé la commande `hugo server`,
votre site est accessible via : `localhost:1313/`.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
