## Présentation

Cet emplacement numérique est le résultat de différentes pérégrinations de la Communauté des Horizons Résilients, un groupe de personnes liées par leur sensibilité sur le sujet de l'[ère anthropocène](https://fr.wikipedia.org/wiki/Anthropoc%C3%A8ne) et d'un potentiel [effondrement de la civilisation industrielle](https://fr.wikipedia.org/wiki/Th%C3%A9ories_sur_les_risques_d%27effondrement_de_la_civilisation_industrielle).

Sous cette forme il présente plusieurs avantages : 

  * La lecture en est plus aisée que de lire de nombreuses conversations. Vous trouverez donc la synthèse d'une discussion sur un sujet donné et à une date donnée.
  * Le contenu de ce site est disponible sur un [dépôt Framagit](https://framagit.org/blankoworld/blankoworld.frama.io)
      * ce qui permet d'en garder un historique complet
      * mais aussi de pouvoir copier l'ensemble du contenu chez soi de manière anonyme (sans s'enregistrer)
      * et de pouvoir l'exécuter chez soi, le modifier, user du résultat comme bon vous semble
      * finalement vous pouvez aussi participer à son évolution en proposant du contenu supplémentaire via le [dépôt Framagit](https://framagit.org/blankoworld/blankoworld.frama.io)
  * On peut dupliquer et publier le contenu sur n'importe quel hébergeur Web qui gère des pages HTML. Tuez le site, et il renaîtra de ses cendres ailleurs :)
  * Si l'outil utilisé par le groupe de discussion est en panne, le site reste statistiquement disponible
  * Le résultat ne dépendant pas d'un code à exécuter à chaque appel, le temps de réponse du site est plus rapide qu'un site habituel

En vous en souhaitant bonne lecture,

L'équipe de rédaction de la Communauté des Horizons Résilients.

