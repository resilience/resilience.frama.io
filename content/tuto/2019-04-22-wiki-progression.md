---
title: "Le site de la communauté : Progression"
subtitle: "Comment apporter du contenu"
tags: [wiki, tutoriel, markdown]
date: 2019-04-22T17:46:32+01:00
author: od
---

Cet espace propose de faire le bilan des discussions entretenues sur le salon Discord. Il utilise des outils particuliers qui apportent souplesse et rapidité de publication.

Ce tutoriel vous facilitera la prise en main de « Progression ».

# Présentation

Ce site utilise 2 outils principaux : 

  * [Hugo](https://gohugo.io/), pour le contenu, l'apparence et la génération du site
  * [Framagit](https://framagit.org/) comme outil pour publier le résultat et pour garder une trace au fil du temps de toutes les modifications apportées. Sorte d'archive

**Vous n'avez cependant pas besoin de maîtriser ces deux outils pour participer au site !**

Participer ne revient qu'à très peu de choses :

  * Apprendre quelques règles de mises en forme, [quasiment les même règles que pour Discord et le format Markdown](https://support.discordapp.com/hc/fr/articles/210298617-Bases-de-la-mise-en-forme-de-texte-Markdown-mise-en-forme-du-chat-gras-italique-soulign%C3%A9-). **Facultatif si vous ne faites qu'écrire des phrases simples regroupées en paragraphes**
  * [Se créer un compte sur Framagit](https://docs.framasoft.org/fr/gitlab/2-creation-configuration-compte.html) **et nous avoir transmis votre pseudo** pour vous autoriser à participer
  * Éditer votre article sur Framagit suivant le présent tutoriel, tout en douceur

# Où se trouvent les articles ?

Tout réside dans un **dossier unique nommé « content »** dont vous pouvez [visualiser le contenu sur le dépôt Framagit de la communauté](https://framagit.org/resilience/resilience.frama.io/tree/master/content).

Chaque dossier représente une section : 

  * page : pour les pages spéciales. Par exemple la page d'à propos,
  * post : pour les billets de news de la communauté,
  * tutos : pour les tutoriels,
  * etc.

Ajoutez donc votre article dans le dossier correspondant.

# C'est quoi un article ?

Dans « Progression », **notre article est un fichier dont l'extension est « md »**. Par exemple *test-article-wiki.md*. Ce dernier contient 2 zones : 

  * l'entête séparée par des tirets au dessus et en dessous : contient des détails de l'article (son titre, date de publication, auteur, etc.)
  * le contenu sous l'entête : au format [Markdown](https://docs.framasoft.org/fr/grav/markdown.html)

Ça ressemble à quelque chose comme : 

```
---
title: Le titre de mon article
date: 2019-04-30
subtitle: "Comment apporter du contenu"
tags: [wiki, tutoriel]
author: od
---

# Présentation

La présentation de ce dont je vais parler.

# Argument 1

## Exemple A

L'exemple donne : 

  * une liste
  * à
  * puce

## Exemple B

Avec : 

1. Une liste
2. à puce
3. ordonnée !
5. c'est pas en ordre
4. même si
6. au départ

# Argument 2

Avec du texte **en gras** mais aussi *en italique*.

# Conclusion

Parfois ça arrive, on conclue !

```

Ce n'est pas compliqué, il faut l'avoir fait une fois pour s'en rendre compte.

# J'utilise quel outil pour éditer l'article ?

Si c'est simplement pour voir à quoi ressemble le texte que vous tapez, utilisez [Dillinger, un éditeur Markdown en ligne](https://dillinger.io/).

Si c'est pour ajouter votre contenu, nous allons utiliser Framagit pour créer l'article.

Cela va se dérouler ainsi : 

  * Création d'un nouveau fichier
  * Ajout du contenu
  * Ajout d'une explication sur l'origine de la création du contenu, la modification d'un fichier existant, etc.
  * Validation
  * Et voilà !

Ainsi l'idéal pour éditer un article rapidement est de le faire sur [notre dépôt de projet Framagit](https://framagit.org/resilience/), plus particulièrement dans le [dossier content du projet de site web](https://framagit.org/resilience/resilience.frama.io/tree/master/content).

Vous voyez le chemin du dossier, quelque chose comme : `resilience.frama.io / content / ` ? Il y a un "+" à côté. S'il n'apparaît pas, c'est que vous avez oublié de [vous inscrire](https://docs.framasoft.org/fr/gitlab/2-creation-configuration-compte.html) **et nous transmettre votre pseudo**.

S'il apparaît, c'est le début de l'aventure ! Cliquez dessus, si si :D. Choisissez **nouveau fichier** et mettez un nom de fichier, par exemple *test.md* (ne jamais oublier le « .md » à la fin).

Ajoutez du contenu, comme l'exemple donné ci-avant sur la structure d'un article.

Puis renseignez le champ **Message de commit** avec une explication sur ce que vous ajoutez. Est-ce un nouvel article ? Une correction d'un article existant ? Des détails d'un canal spécifique à une date spécifique sur notre Discord ?

Finalement validez la modification en **cliquant sur le bouton « Commit changes »**.

Votre article est publié, il sera mis en ligne au bout de quelques secondes. Si ce n'est pas le cas, signalez-le sur le Discord.

# Liens utiles

  * [S'inscrire à Framagit](https://docs.framasoft.org/fr/gitlab/2-creation-configuration-compte.html)
  * [Documentation (en) d'Hugo](https://gohugo.io/documentation/)
  * [Le langage Markdown par Framasoft](https://docs.framasoft.org/fr/grav/markdown.html) (tout n'est **pas** forcément valable)
  * [Les extensions Markdown que nous avons à disposition](https://github.com/russross/blackfriday#extensions)
