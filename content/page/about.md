---
title: À propos de nous
subtitle: Pourquoi vous devriez nous porter de l'intérêt
comments: false
---

Nous sommes la Communauté des Horizons Résilients. Nous nous sommes réunis autour d'un constat simple : le monde actuel ([ère de l'Anthropocène](https://fr.wikipedia.org/wiki/Anthropoc%C3%A8ne)) - s'il continu à ce rythme - va à sa perte. Nous souhaitons engager le pas vers une transition progressive vers un mode de vie alternatif et autonome. Que ce soit en nourriture, en électricité, en eau et en produits permettant de vivre dans de bonnes conditions et permettant un épanouissement des personnes.

L'ensemble des contenus proposés ici vont donc dans ce sens et permettent d'en savoir plus sur la progression de notre réflexion et de nos recherches.
